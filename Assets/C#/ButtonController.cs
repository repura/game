﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ButtonController : MonoBehaviour
{public void ChangeScene(string scene)
    {
        switch (scene)
        {
            case "3D":
                SceneManager.LoadScene("3D");
                break;

            case "2D":
                SceneManager.LoadScene("2D");
                break;
            default:
                SceneManager.LoadScene("Title");
                break;
        }
    }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
