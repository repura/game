﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager2D : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Play(); 
    }
    public void Play()
    {
        var field = GameObject.Find("Field").GetComponent<Field>();

        // 行数、列数、爆弾の個数を指定してフィールドを初期化します。
        field.Initialize(9, 9, 10);
    }
    // Update is called once per frame
    void Update()
    {
        
    }
}
